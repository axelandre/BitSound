@extends('layouts.app')

@section('content')
    <h1>{{$utilisateur->name}}</h1>
    @auth
        @if(Auth::user()->id != $utilisateur->id)
            <a data-pjax-toggle href="/changersuivi-{{$utilisateur->id}}">
            @if(Auth::user()->JeLesSuit->contains($utilisateur->id))
                <div class="ui labeled button" tabindex="0">
                    <div class="ui red button">
                        <i class="heart icon"></i>Ne plus suivre
                    </div>
                    <a href="#" class="ui basic red left    pointing label">
                        {{$nb_followers}}
                    </a>
                </div>
            @else
                <div class="ui labeled button" tabindex="0">
                    <div class="ui red button">
                        <i class="heart icon"></i>Suivre
                    </div>
                    <a href="#" class="ui basic red left    pointing label">
                        {{$nb_followers}}
                    </a>
                </div>
            @endif
            </a>
        @endif
    @endauth
    <h2>{{Auth::user()->id == $utilisateur->id ? "Mes":"Ses"}} musiques :</h2>


    @include('musique',['musique'=>$utilisateur->chansons])

@endsection