<html>
<head>
    <meta charset='UTF-8'>
    <title>{{$title}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>

@yield('content')
</body>



</html>