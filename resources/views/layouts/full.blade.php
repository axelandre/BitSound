<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BitSound') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="{{asset('semantic/dist/semantic.js')}}"></script>
    <link rel="stylesheet" href="{{asset('semantic/dist/semantic.css')}}">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/music.css')}}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">
</head>
<body>
<div class="modalu">
    <i class="close icon" ></i>

        <form method="POST" action="/creer-chanson" enctype="multipart/form-data" class="ui column grid form divided grid">
            <div class="eight wide column">
                {{ csrf_field() }}
                <div class="field ">
                    <label>Nom de la musique</label><input type="text" name="nom" required>
                </div>
                <div class="field">
                    <label>Style</label><input type="text" name="style" required>
                </div>

                <div class="field">
                    <label for="chanson" class="positive ui button">Envoyer votre pochette</label>
                    <input type="file" name="chanson" id="chanson" class="positive ui button"  required style="display: none"><br>
                </div>
                <div class="field">
                    <label for="cover" class="positive ui button">Envoyer votre musique</label>
                    <input type="file" name="cover" id="cover" class="positive ui button"  required style="display: none"><br>
                </div>
                <div class="field">
                    <div class="ui  action input">
                        <button class="ui positive labeled icon button">
                            <i class="checkmark icon"></i>
                            Valider
                        </button>
                    </div>
                </div>
            </div>
            <div class="column eight wide">
                <h2 class="text-center">Postez vos propres musiques!</h2>
                <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque ducimus eum eveniet excepturi fuga ipsum nobis obcaecati officia quam, ratione? Accusamus aperiam cupiditate et ex exercitationem impedit itaque vel voluptatibus.</p>
            </div>

        </form>
</div>

@guest
    <header>
        <nav class="navigation">
            <a data-pjax href="/home"><img src="img/logo.png" alt=""></a>
            <form id="search" action="" method="">
                <input type="search" name="search" id="" placeholder="Rechercher"/>
                <input type="submit" value="" hidden/>
            </form>
            <div class="log">
                <a data-pjax href="{{route('login')}}">Se <connecter> </connecter></a>
                <a data-pjax href="{{route('register')}}">S'inscrire</a>
            </div>

        </nav>
    </header>
    @else
        <header>
            <nav class="navigation">
                <a data-pjax href="/"><img src="{{asset('img/logo.png')}}" alt=""></a>
                <form id="search" action="" method="">
                    <input type="search" name="search" id="" placeholder="Rechercher"/>
                    <input type="submit" value="" hidden/>
                </form>
                <div class="log">

                    <a href="" class="user">{{Auth::user()->name}}</a>
                    <a href="#" id="menu"><i class="fas fa-bars"></i></a>

                </div>

            </nav>

        </header>
        <aside>
            <ul>
                <li><a data-pjax href="/utilisateur-{{Auth::id()}}">Mes Musiques</a></li>
                <li data-pjax class=""><a data-pjax href="/all-playlists">Mes Playlists</a></li>
                <li data-pjax id="add_music">Ajouter une musique</li>
                <li><a class="item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Déconnexion</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form></li>
            </ul>
        </aside>
        <div class="overlay"></div>
        </div>



        @endguest

<div class="contenu" id="pjax-container">
    @yield('content')
</div>

        <div class="player">
            <audio id="audio" src="lol.mp3" controls item-width="100%"></audio>
        </div>


<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/jquery.pjax.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
</body>
</html>
