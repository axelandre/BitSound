@extends('layouts.app')
@section('content')
    @if($utilisateurs)
    <table class="ui celled striped table">
        <thead>
        <tr><th colspan="3">
                Utilisateurs
            </th>
        </tr></thead>
        <tbody>
        @foreach($utilisateurs as $u)
            <tr>
                <td class="collapsing">
                    <a data-pjax href="/utilisateur-{{$u->id}}">{{$u->name}}</a>
                </td>
                <td>{{$u->email}}</td>
                <td><a data-pjax-toggle href="/changersuivi-{{$u->id}}">{{Auth::user()->JeLesSuit->contains($u->id)?"Ne plus suivre":"Suivre"}}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif
    @include('musique',['musique'=>$chansons])

@endsection