@extends('layouts.app')
@section('content')
    <table class="ui inverted violet table collapsing">
        <thead>
        <tr><th colspan="3">
                Vos playlists
            </th>
        </tr></thead>
        <tbody>
        @foreach($playlists as $p)
            <tr>
                <td >
                    <a data-pjax href="/playlist-{{$p->id}}">{{$p->nom}}</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection