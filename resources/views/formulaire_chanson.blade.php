@extends("layouts.app")
@section('content')
<div class="container">
    <form method="POST" action="/creer-chanson" enctype="multipart/form-data" class="ui column grid form divided grid">
        <div class="eight wide column">
        {{ csrf_field() }}
        <div class="field ">
        <label>Nom de la musique</label><input type="text" name="nom" required>
        </div>
        <div class="field">
        <label>Style</label><input type="text" name="style" required>
        </div>

        <div class="field">
            <label for="chanson" class="positive ui button">Parcourir</label>
        <input type="file" name="chanson" id="chanson" class="positive ui button"  required style="display: none"><br>
            </div>
        <div class="field">
            <div class="ui  action input">
                <button class="ui positive labeled icon button">
                    <i class="checkmark icon"></i>
                    Valider
                </button>
            </div>
        </div>
        </div>
        <div class="column eight wide">
            <h2 class="text-center">Postez vos propres musiques!</h2>
            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque ducimus eum eveniet excepturi fuga ipsum nobis obcaecati officia quam, ratione? Accusamus aperiam cupiditate et ex exercitationem impedit itaque vel voluptatibus.</p>
        </div>

    </form>
</div>
@endsection