
<div class="all-music">
    @foreach($musique as $m)
        <div  class="music">
            <a class="music-launcher">
                <div class="music-panel" data-value="{{$m->fichier}}">
                    <p class="music-panel__title">{{$m->nom}}</p>
                    <p class="music-panel__author">{{$m->utilisateur->name}}</p>
                    <i class="play circle icon"></i>
                    <div class="music-panel__overlay"></div>
                    <a href="#" class="music-panel__play">></a>
                    <img src="{{$m->cover==null?'http://lorempixel.com/g/200/200/':$m->cover}}" alt="" class="music-panel__cover">
                    <p class="genre">{{$m->style}}</p>

                </div>
            </a>
            <a href="#" class="music-panel__addto">+</a>
            @if(Auth::id()==$m->utilisateur->id)
                <form action="/delete" method="post">
                    {{ csrf_field() }}
                    <input type="text" value="{{$m->id}}" name="chanson" hidden>
                    <input type="submit" class="music-panel__delete" value="x">

                </form>
            @endif
            <div class="undercard"></div>
            <ul class="playlists">
                <input type="text" class="addAPlaylist" placeholder="Ajouter une Playlist"><a href="#">+</a>
                <hr>
                <li>Moi <a href="#">+</a></li>
                <li>Moi <a href="#">+</a></li>
                <li>Moi <a href="#">+</a></li>
                <li>Moi <a href="#">+</a></li>
                <li>Moi <a href="#">+</a></li>
            </ul>
        </div>
    @endforeach
</div>
