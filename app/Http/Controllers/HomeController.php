<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chanson;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $liste_chanson = Chanson::orderBy('post_date', 'desc')->get();
        return view('home',["chansons"=>$liste_chanson]);
    }
}
