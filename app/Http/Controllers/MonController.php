<?php

namespace App\Http\Controllers;

use App\Chanson;
use App\Playlist;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MonController extends Controller
{
    public function formulaire_chanson()
    {
        return view("formulaire_chanson", ["title" => "Formulaire de chanson"]);

    }

    public function creerchanson(Request $request)
    {

        if ($request->hasFile('chanson') && $request->file('chanson')->isValid()) {
            $c = new Chanson();
            $c->nom = $request->input('nom');
            $c->style = $request->input('style');
            $c->utilisateur_id = Auth::id();
            $c->fichier = $request->file("chanson")->store("public/chansons/". Auth::id());
            $c->fichier = str_replace("public/", "/storage/", $c->fichier);
            if ($request->hasFile('cover')&& $request->file('cover')->isValid()){
                $c->cover = $request->file("cover")->store("public/cover/". Auth::id());
                $c->cover = str_replace("public/", "/storage/", $c->cover);
            }
            $c->save();
        }
        return redirect('/');
    }

    public function utilisateur($id)
    {
        $playlists = Playlist::where('utilisateur_id',$id)->get();
        $utilisateur = User::find($id);
        $musiques = $utilisateur->chansons();
        $nb_musiques = count($musiques);
        $nb_followers = count($utilisateur->ilsMeSuivent);
        if ($utilisateur == false)
            abort(404);
        return view('utilisateur', ["musiques" => $musiques, "utilisateur" => $utilisateur, "nb_musiques" => $nb_musiques,'playlists'=>$playlists,"nb_followers"=>$nb_followers]);
    }
    public function playlist($id)
    {
        $playlist = Playlist::find($id);
        $musiques = $playlist->chansons;
        if ($playlist == false)
            abort(404);
        return view('playlist', ["chansons" => $musiques,"name"=>$playlist->nom]);
    }

    public function changerSuivi(Request $r, $id)
    {
        $utilisateur = User::find($id);
        if ($utilisateur == false)
            abort(403);
        Auth::user()->jeLesSuit()->toggle($id);
        return back();

    }
    public function search($s){
        $users = User::whereRaw("name LIKE CONCAT(?,'%')",[$s])->get();
        $chansons = Chanson::whereRaw("nom LIKE CONCAT(?,'%')",[$s])->get();
        return view('recherche',["utilisateurs"=>$users,"chansons"=>$chansons]);
    }
    public function delete(Request $request){
        $chanson = Chanson::find($request->input('chanson'));
        if($chanson->utilisateur_id!=Auth::id());
        Chanson::destroy($chanson->id);
        return back();


    }
    public function all_playlists(){
        $playlists = Auth::user()->playlists;
        return view('all-playlists',["playlists"=>$playlists]);
    }
    public function addPlaylist($s){
        $playlist = new Playlist();
        $playlist->nom = $s;
        $playlist->utilisateur_id = Auth::id();
        $playlist->save();
        return back();
    }
}
