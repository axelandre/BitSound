<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/formulaire-chanson', 'MonController@formulaire_chanson')->middleware('auth')->name('formulaire-chanson');
Route::post('/creer-chanson',"MonController@creerchanson")->middleware('auth');
Route::post('/delete',"MonController@delete")->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/utilisateur-{id}','MonController@utilisateur')->where('id','[0-9]+');
Route::get('/playlist-{id}','MonController@playlist')->where('id','[0-9]+')->middleware('auth')->name('playlist');
Route::get('/all-playlists','MonController@all_playlists')->middleware("auth");
Route::get('/changersuivi-{id}','MonController@changerSuivi')->middleware('auth')->where('id','[0-9]+');
Route::get('/search/{s}','MonController@search');
Route::get('/add-{s}','MonController@addPlaylist');

