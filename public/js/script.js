$(document).pjax('[data-pjax] a, a[data-pjax]', '#pjax-container');
$(document).pjax('[data-pjax-toggle] a, a[data-pjax-toggle]', '#pjax-container',{push:false});
$(document).ready(function () {
    function ToogleMenu() {
        $('aside').toggleClass('active');
        $('.overlay').toggleClass('active');
    }
    $('p').click(function () {
        ToogleMenu();
    });
    $('li').click(function () {
        ToogleMenu();
    });
    $('.overlay').click(function(){
        ToogleMenu();
    });
    $('#menu').click(function (e) {
        ToogleMenu();
        e.preventDefault();
    });
    $('.music-panel').on('click',function (e) {
        e.preventDefault();
        var file = $(this).attr('data-value');
        var audio = $('#audio');
        console.log(file);
        audio.attr('src',file);
        audio[0].load();
        audio[0].play();
    });

    function ToogleModalu(){
        $('.modalu').toggleClass('active');
    }
    $('#add_music').click(function () {
        ToogleModalu();
    });
    $('.close.icon').click(function () {
        ToogleModalu();
    });
    $('.music-panel__addto , .playlists').mouseover(function () {
        $(this).parent().find('.playlists').addClass('active');

    });

    $('.music-panel__addto, .playlists').mouseleave(function () {
        $(this).parent().find('.playlists').removeClass('active');

    });
    $(".contenu").on('keyup','.addAPlaylist',function (e) {
        e.preventDefault();
        $(this).siblings('a').attr('href',"/add-"+$(this).val());

    });

});